drop table if exists `user`;

create table user (
    id int(11) not null auto_increment,
    email varchar(100) not null,
    password varchar(100) not null,
    status tinyint(1) not null default 0,
    activation_date datetime default null,
    create_date datetime default current_timestamp,
    update_date datetime default current_timestamp on update current_timestamp,
    primary key (id),
    KEY `user_index_email` (`email`),
    KEY `user_index_status` (`status`),
    KEY `user_index_activation_date` (`activation_date`),
    KEY `user_index_creation_date` (`create_date`),
    KEY `user_index_update_date` (`update_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
