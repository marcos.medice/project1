package com.project.api.model.type;

public enum StatusType {
    NOT_ACTIVATED(0),
    ACTIVE(1),
    TIMED_OUT(2),
    BANNED(3);

    private Integer value;

    StatusType(Integer value) { this.value = value; }

    public Integer getValue() { return this.value; }
}
