package com.project.api.util;

import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class PasswordEncoder {

    @Bean
    public static String encodePassword(String password) {
        if(password == null) {
            throw new NullPointerException("Null password input");
        }
        return new BCryptPasswordEncoder().encode(password);
    }
}