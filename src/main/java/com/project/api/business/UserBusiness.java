package com.project.api.business;

import com.project.api.model.entity.User;
import com.project.api.repository.UserRepository;
import org.hibernate.service.spi.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserBusiness {

    @Autowired
    private UserRepository userRepository;

    public User saveUser(User user) throws ServiceException {

        if(user == null) {
            throw new NullPointerException("Null user input");
        }

        User userResponse = this.userRepository.save(user);

        return userResponse;
    }
}
