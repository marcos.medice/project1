package com.project.api.controller.model;

import java.time.LocalDateTime;

public class UserResponse {

    private String email;
    private Boolean status;
    private LocalDateTime activationDate;

    public UserResponse() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public LocalDateTime getActivationDate() {
        return activationDate;
    }

    public void setActivationDate(LocalDateTime activationDate) {
        this.activationDate = activationDate;
    }

    @Override
    public String toString() {
        return "UserResponse{" +
                "email='" + email + '\'' +
                ", status=" + status +
                ", activationDate=" + activationDate +
                '}';
    }
}
