package com.project.api.controller;

import com.project.api.business.UserBusiness;
import com.project.api.controller.model.UserPostRequest;
import com.project.api.controller.model.UserResponse;
import com.project.api.model.entity.User;
import com.project.api.model.type.StatusType;
import com.project.api.util.PasswordEncoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/user")
@RestController
public class UserController {

    private final Logger LOG = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private UserBusiness userBusiness;

    @PostMapping(value = "/users", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<UserResponse> postUser(@RequestBody UserPostRequest request) {

        LOG.debug("REQUEST postUser: {}", request);

        User savedUser;
        try {
            User user = new User();
            user.setEmail(request.getEmail());
            user.setPassword(PasswordEncoder.encodePassword(request.getPassword()));
            user.setStatus(StatusType.NOT_ACTIVATED.getValue());
            savedUser = this.userBusiness.saveUser(user);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        LOG.debug("RESPONSE postUser: {}", savedUser);

        return new ResponseEntity<>(HttpStatus.OK);
    }

}
